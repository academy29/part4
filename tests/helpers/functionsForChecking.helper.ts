import { expect } from 'chai';
const articleSchemas = require('../api/specs/data/articleSchemas_testData.json');
const authorSchemas = require('../api/specs/data/authorSchemas_testData.json');
const studentSchemas = require('../api/specs/data/studentSchemas_testData.json');
const coursesSchemas = require('../api/specs/data/coursesSchemas_testData.json');

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 |403 | 404 | 409 | 500){
    expect(response.statusCode, `Status code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseBodyStatus(response, status: string){
    expect(response.body.status, `Status should be ${status}`).to.be.equal(status);
}

export function checkResponseBodyMessage(response, message: string){
    expect(response.body.message, `Message should be ${message}`).to.be.equal(message);
}

export function checkResponseTime(response, maxResponseTime: number = 3000){
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}`)
        .to.be.lessThan(maxResponseTime);
}

export function checkArticleSchema(response, jsonSchemaName){
    expect(response.body).to.be.jsonSchema(articleSchemas[jsonSchemaName]);
}

export function checkAuthorSchema(response, jsonSchemaName){
    expect(response.body).to.be.jsonSchema(authorSchemas[jsonSchemaName]);
}

export function checkStudentSchema(response, jsonSchemaName){
    expect(response.body).to.be.jsonSchema(studentSchemas[jsonSchemaName]);
}

export function checkCoursesSchema(response, jsonSchemaName){
    expect(response.body).to.be.jsonSchema(coursesSchemas[jsonSchemaName]);
}