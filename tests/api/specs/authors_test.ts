import {expect } from "chai"; 
import { withoutBody } from "got/dist/source";
import { AuthorController } from "../lib/controllers/author.controller";
import { checkResponseTime, checkStatusCode, checkAuthorSchema} from "../../helpers/functionsForChecking.helper";

var chai = require('chai');
chai.use(require('chai-json-schema'));
// globals
let userId;
let authorId;

//////////////

const author = new AuthorController();

describe("Author controller", () => {

    before("Author passes authentication successfully", async () => {
        //Authenticating
        let response = await author.userLogin("vladik_sexy@i.ua", "1nv1s1ble");
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkAuthorSchema(response, "schema_authorLogin");

        //Getting user id
        response = await author.getUserId();
        userId = response.body.id;

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkAuthorSchema(response, "schema_userInfo");
    });

    it("Author can get basic info", async () => {
        let response = await author.getAuthorInfo();
        authorId = response.body.id;

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkAuthorSchema(response, "schema_authorInfo");
    });

    it("Setting author's basic info", async () => {
        let authorObj = {
            id: authorId,
            userId: userId,
            avatar: null,
            firstName: "Professor Severus",
            lastName: "Snape",
            job: "Future Defense against the Dark Arts Master",
            location: "Algeria",
            company: "Hogwarts",
            website: "www.hotwitch.com",
            twitter: "https://twitter.com/crazy_potions_master",
            biography: "How I became a Dumbledore's spy"
        };
                
        let response = await author.setAuthorInfo(authorObj);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkAuthorSchema(response, "schema_authorProfileUpdate");        
    });

    it("User can get author's detailed overview", async () => {
        let response = await author.getAuthorsOverview(authorId);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkAuthorSchema(response, "schema_authorOverview");
    });

    after(function () {
        console.log("\t Author controller tests finished");
    });
}
);