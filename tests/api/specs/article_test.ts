import {expect } from "chai"; 
import { withoutBody } from "got/dist/source";
import { ArticleController } from "../lib/controllers/article.controller";
import { checkResponseTime, checkStatusCode, checkArticleSchema } from "../../helpers/functionsForChecking.helper";

var chai = require('chai');
chai.use(require('chai-json-schema'));

const article = new ArticleController();

describe("Article controller", () => {

    before("User is authenticated to access articles functionality", async () => {
        let response = await article.userLogin("vladik_sexy@i.ua", "1nv1s1ble");
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });

    it("Author can create a new article successfully", async () => { 
        let articleName = 'Test new article';
        let articleText = 'some testing text';
        let response = await article.createNewArticle(articleName, articleText);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkArticleSchema(response, "schema_newArticle");

        expect(response.body.name, 'Article Title is not saved correctly').to.be.equal(articleName);
        expect(response.body.text, 'Article text is not saved correctrly').to.be.equal(articleText);
    });

    it("Author can get a list of his/her articles", async () => {
        let response = await article.getArticlesByAuthor();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkArticleSchema(response, "schema_allArticlesByAuthor");
    });

    it("User can get an article by Id", async () => {
        const articleId = 'ff405e10-5787-41b1-b109-0a9514d58587';

        let response = await article.getArticleById(articleId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkArticleSchema(response, "schema_articleById");
    });

    // Using test data

    let commentsTestDataSet = [
        { articleId: 'ff405e10-5787-41b1-b109-0a9514d58587', commentText: 'Nox' },
        { articleId: 'a2a6b375-e8ea-489e-8433-ec814864738d', commentText: 'Protego' },
        { articleId: 'ee574748-bdd1-48af-ac57-07697fd91c61', commentText: 'Alohomora' },
        { articleId: '87ec3090-3d59-413f-bb8f-3e87c90dde56', commentText: 'Imperio' },
        { articleId: '6cdaaa80-70c0-4d37-b08a-d35558fa4368', commentText: 'Crucio' },
        { articleId: '3a3747ea-ce9a-450e-b15d-3116c32fed33', commentText: 'Sectumsepmra' },
    ];

    commentsTestDataSet.forEach((comment) => {
        it(`Posting a comment with the text "${comment.commentText}"`, async () => {
            let response = await article.postArticleComment(comment.articleId, comment.commentText);

            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);
            checkArticleSchema(response, "schema_postComment");
    
            expect(response.body.text, 'Comment text is not saved correctrly').to.be.equal(comment.commentText);
        });
    });

    // Test Data for articles
    let articleIdsDataSet = [
        'ff405e10-5787-41b1-b109-0a9514d58587',
        '3a3747ea-ce9a-450e-b15d-3116c32fed33',
        '6cdaaa80-70c0-4d37-b08a-d35558fa4368',
        'a2a6b375-e8ea-489e-8433-ec814864738d',
        'ee574748-bdd1-48af-ac57-07697fd91c61',
        '87ec3090-3d59-413f-bb8f-3e87c90dde56'
    ];

    articleIdsDataSet.forEach( (id) => {
        it(`Getting comments by article id: ${id} `, async () => {
                        
            let response = await article.getCommentsByArticleId(id);
    
            checkStatusCode(response, 200);
            checkResponseTime(response, 3000);
            checkArticleSchema(response, "schema_commentsByArticleId");
    
            for(let i = 0; i<response.body.length; i++){
                expect(response.body[i].articleId, 'Article id of comment expected to correstpond').to.be.equal(id);
            }
        });
    });

    // Negative tests
    // THIS TEST WILL FAIL
    it("Author should not be able to create an article with empty title and text", async () => { 
        let articleName = '';
        let articleText = '';
        let response = await article.createNewArticle(articleName, articleText);
        
        checkStatusCode(response, 400);
        checkResponseTime(response, 3000)
        checkArticleSchema(response, "schema_newArticle");
        
        expect(response.body.name, 'Article Title is not saved correctly').to.be.equal(articleName);
        expect(response.body.text, 'Article text is not saved correctrly').to.be.equal(articleText);
    });

    // another negative test (it will not fail)
    it("User cannot post a comment to a non-existing article", async () => {
        const articleId = 'a437510a-acf9-4330-a5cc-140e0cde614a';// this article does not exist
        const commentText = "Wingardium LeviOsa, not LeviosA!!"
        let response = await article.postArticleComment(articleId, commentText);

        checkStatusCode(response, 404);
        checkResponseTime(response, 3000);
        checkArticleSchema(response, "schema_articleNotFound");
    });   
    after(function () {
        console.log("\t Article controller tests finished");
    });
});