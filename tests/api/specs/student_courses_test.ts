import {expect } from "chai"; 
import { withoutBody } from "got/dist/source";
import { StudentController } from "../lib/controllers/student.controller";
import { CoursesController } from "../lib/controllers/courses.controller";
import { checkResponseTime, checkStatusCode, checkStudentSchema, checkCoursesSchema} from "../../helpers/functionsForChecking.helper";

var chai = require('chai');
chai.use(require('chai-json-schema'));
// globals
let userId;
//////////////

const student = new StudentController();
const courses = new CoursesController();


describe("Student controller", () => {
    before("Student passes authentication successfully", async () => {
        //Authenticating
        let response = await student.userLogin("vlad.cpp@gmail.com", "1nv1s1ble");
        
        courses.setStudentToken(response.body.accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkStudentSchema(response, "schema_studentLogin");

        //Getting userId for Student
        response = await student.getUserId();
        userId = response.body.id;

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkStudentSchema(response, "schema_userInfo");
    });


    it("Student can mark all notifications as read", async () => {
        let response = await student.readAllNotifications(userId);
                
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });
});

describe("Courses controller", () => {

    it("User is able to get his/her courses", async () => {
        let response = await courses.getMyCourses();
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkCoursesSchema(response, "schema_getMyCourses");
    }); 

    it("User is able to get a course by Id", async () => {
        let response = await courses.getCourseInfoById('aaa449e4-01e1-49e6-bf54-e8056f58508a');
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkCoursesSchema(response, "schema_courseInfoById");
    }); 

    // SOME NEGATIVE TESTS FINALLY
    it("User cannot access a course by a wrong Id", async () => {
        let response = await courses.getCourseInfoById('abrakadabra');

        checkStatusCode(response, 400); // I expect an error here
        checkResponseTime(response, 3000);
    }); 

    after(function () {
        console.log("\t Student & Courses controller tests finished");
    });
});





