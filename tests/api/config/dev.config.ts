global.appConfig = {
    envName: "DEV Environment",
    baseUrl: 'https://knewless.tk/api',
    swaggerUrl: 'https://knewless.tk/api/swagger-ui/index.html',

    users: {
        Vlad: {
            email: 'vlad.cpp@gmail.com',
            password: '1nv1s1ble'
        },
    },
};