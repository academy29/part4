import { ApiRequest } from "../request";

//globals
const prefixUrl = global.appConfig.baseUrl;
let imageUrl = 'https://knewless.tk/assets/images/f8d72d26-e21c-422e-be25-67c6c69a8cc1.jpg';
let accessToken = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2NjA2YzVjMS01OTJlLTRiYjQtYTAyZi1kMWY1MDljMmUzOWUiLCJpYXQiOjE2NTg3NDU4MjIsImV4cCI6MTY1ODgzMjIyMn0.2AjlBhBh-AQYrwjDqHMFEEq52SwHJ4fWqi9tM0kJKTRhPfjJd14h8IKhPmoMJxISjh1-uhZsQXR8wzUHeDwWmg';

export class ArticleController {


    // POST ../api/auth/login path (NOT api/login as described!)
    async userLogin(login, pass){
        
        let credentials = {
            email: login,
            password: pass
        };
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('POST')
            .url('auth/login')
            .body(credentials)
            .send();
        this.setAuthorToken(response.body.accessToken);
        return response;
    }

    async setAuthorToken(token) {
        accessToken = token;
    }

    //POST /api/article path
    async createNewArticle(articleName, articleText){
        let articleObj = {
            name: articleName,
            image: imageUrl,
            text: articleText,
            uploadImage: {}
        };
        
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('POST')
            .url('article/')
            .bearerToken(accessToken)
            .body(articleObj)
            .send();

        return response;
    }     

    //GET api/article/author path
    async getArticlesByAuthor(){
        
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('article/author')
            .bearerToken(accessToken)
            .send();

        return response;
    } 

    //GET api/article/{articleId} path
    async getArticleById(artId){
        
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('article/' + artId)
            .bearerToken(accessToken)
            .send();

        return response;
    } 

    //POST /api/article_comment path
    async postArticleComment(id, commentText){
        let commentObj = {
            articleId: id,
            text: commentText
        };
        
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('POST')
            .url('article_comment')
            .bearerToken(accessToken)
            .body(commentObj)
            .send();

        return response;
    } 
    //GET api/article_comment/of/:articleId path
    async getCommentsByArticleId(artId){
        
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('article_comment/of/' + artId)
            .bearerToken(accessToken)
            .send();

        return response;
    } 

}