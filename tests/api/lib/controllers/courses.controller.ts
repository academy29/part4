import { ApiRequest } from "../request";

const prefixUrl = global.appConfig.baseUrl;
let accessToken;

export class CoursesController {

    //GET api/course/continue path
    async getMyCourses(){
    
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('course/continue')
            .bearerToken(accessToken)
            .send();

        return response;
    } 
    async setStudentToken(token) {
        accessToken = token;
    }
    //GET api/course/continue path
    async getCourseInfoById(courseId){
    
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('course/' + courseId +'/info')
            .bearerToken(accessToken)
            .send();

        return response;
    } 
}