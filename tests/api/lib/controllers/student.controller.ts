import { ApiRequest } from "../request";

const prefixUrl = global.appConfig.baseUrl;
let accessToken;

export class StudentController {

    // POST ../api/auth/login path (NOT api/login as described!)
    async userLogin(login, pass){
        
        let credentials = {
            email: login,
            password: pass
        };
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('POST')
            .url('auth/login')
            .body(credentials)
            .send();
        this.setStudentToken(response.body.accessToken);
        return response;
    }

    async setStudentToken(token) {
        accessToken = token;
    }

    // GET ../api/user/me path
    async getUserId(){
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('user/me')
            .bearerToken(accessToken)
            .send();

        return response;
    }   

    //PUT notification/read-all?userId= path
    async readAllNotifications(uId){
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('PUT')
            .url('notification/read-all?userId='+uId)
            .bearerToken(accessToken)
            .send();

        return response;
    } 
}