import { ApiRequest } from "../request";

const prefixUrl = global.appConfig.baseUrl;
let accessToken;

export class AuthorController {

    // POST ../api/auth/login path (NOT api/login as described!)
    async userLogin(login, pass){
        
        let credentials = {
            email: login,
            password: pass
        };
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('POST')
            .url('auth/login')
            .body(credentials)
            .send();
        this.setAuthorToken(response.body.accessToken);
        return response;
    }

    async setAuthorToken(token) {
        accessToken = token;
    }
    // GET ../api/user/me path
    async getUserId(){
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('user/me')
            .bearerToken(accessToken)
            .send();

        return response;
    }   

    // GET ../api/author/ path
    async getAuthorInfo(){
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('author/')
            .bearerToken(accessToken)
            .send();
        
        return response;
    }
    
    // POST ../api/author/ path
    async setAuthorInfo(authorObj){
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('POST')
            .url('author/')
            .bearerToken(accessToken)
            .body(authorObj)
            .send();
        return response;
    }   

    // GET ../api/author/overview/{userId} path
    async getAuthorsOverview(authorId){
        const response = await new ApiRequest()
            .prefixUrl(prefixUrl)
            .method('GET')
            .url('author/overview/' + authorId)
            .bearerToken(accessToken)
            .send();
        return response;
    }    
}